# Backend and documentation [here](https://gitlab.com/todo14/todo-backend)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run end-to-end tests
```
npm run test:e2e
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
