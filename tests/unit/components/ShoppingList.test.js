import Vue from 'vue';
import Vuetify from 'vuetify';
import { shallowMount } from '@vue/test-utils';
import ShoppingList from '@/components/ShoppingList/ShoppingList.vue';

import { items } from '../testdata/common';

Vue.use(Vuetify);

beforeEach(() => {
  fetch.resetMocks();
});

jest.mock('@/services/task', () => jest.fn(() => JSON.stringify({ test: 'test' })));

describe('ShoppingList.vue', () => {
  it('renders and calls to fetch shoppinglist items', () => {
    fetch.mockResponseOnce(
      JSON.stringify([items]),
      {
        status: 200,
        headers: { 'content-type': 'application/json' },
      },
    );
    const wrapper = shallowMount(ShoppingList);

    expect(wrapper.html()).toContain('Quick add');
    expect(fetch.mock.calls.length).toBe(1);
  });
  // mount throws error, no idea why, no solution found
  // it('add-button triggers fetch call to post an item', async () => {
  //   fetch.mockResponseOnce(
  //     JSON.stringify({ data: items }),
  //     {
  //       status: 200,
  //       headers: { 'content-type': 'application/json' }
  //     }
  //   )

  //   const wrapper = mount(ShoppingList)

  //   wrapper.find('.input-button').simulate('click')
  //   expect(fetch.mock.calls.length).toBe(1)
  // })
});
