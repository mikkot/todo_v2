const items = [
  {
    name: 'first',
  },
  {
    name: 'second',
  },
];

const item = {
  name: 'name',
};

const user = {
  id: 5,
};

const task = {
  name: 'name',
};

const tasks = [
  { name: 'first' },
  { name: 'second' },
];

export {
  items,
  item,
  user,
  task,
  tasks,
};
