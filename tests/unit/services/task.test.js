import taskService from '@/services/task';
import { task, tasks } from '../testdata/common';

beforeEach(() => {
  fetch.resetMocks();
});

describe('Services - TASK', () => {
  describe('postTask', () => {
    it('postTask returns error string if status is not 200-300', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: task }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const res = await taskService.postTask({ task });
      expect(res).toEqual('Error');
      expect(fetch.mock.calls.length).toEqual(1);
    });

    it('postTask returns the created task if request succeeds', async () => {
      fetch.mockResponseOnce(JSON.stringify({ data: task }));

      const res = await taskService.postTask({ task });
      expect(res.data).toEqual(task);
      expect(fetch.mock.calls.length).toEqual(1);
    });
  });

  describe('deleteTask', () => {
    it('deleteTask returns error string if status is not 200-300', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: task }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const res = await taskService.deleteTask({ task });
      expect(res).toEqual('Error');
      expect(fetch.mock.calls.length).toEqual(1);
    });

    it('deleteTask returns success string if request succeeds', async () => {
      fetch.mockResponseOnce('{}');

      const res = await taskService.deleteTask({});
      expect(res).toEqual('Success');
      expect(fetch.mock.calls.length).toEqual(1);
    });
  });

  describe('getTask', () => {
    it('getTask returns error string if status is not 200-300', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: task }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const res = await taskService.getTask({});
      expect(res).toEqual('Error');
      expect(fetch.mock.calls.length).toEqual(1);
    });

    it('getTask returns a single task if found', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: task }),
        {
          status: 200,
          headers: { 'content-type': 'application/json' },
        },
      );

      const res = await taskService.getTask({ id: '12' });
      expect(res.data).toEqual(task);
      expect(fetch.mock.calls.length).toEqual(1);
    });
  });

  describe('getTasks', () => {
    it('getTasks returns error string if status is not 200-300', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: task }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const res = await taskService.getTasks({ task });
      expect(res).toEqual('Error');
      expect(fetch.mock.calls.length).toEqual(1);
    });

    it('getTasks returns an array of tasks if found', async () => {
      fetch.mockResponseOnce(JSON.stringify({ data: tasks }));

      const res = await taskService.getTasks();
      expect(res.data).toEqual(tasks);
      expect(fetch.mock.calls.length).toEqual(1);
    });
  });

  describe('updateTask', () => {
    it('updateTask returns error string if status is not 200-300', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: task }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const res = await taskService.updateTask({ task });
      expect(res).toEqual('Error');
      expect(fetch.mock.calls.length).toEqual(1);
    });

    it('updateTask returns the updated task if request succeeds', async () => {
      fetch.mockResponseOnce(JSON.stringify({ data: task }));

      const res = await taskService.updateTask({ task });
      expect(res.data).toEqual(task);
      expect(fetch.mock.calls.length).toEqual(1);
    });
  });
});
