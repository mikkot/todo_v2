import shoppinglistService from '@/services/shoppinglist';
import localStorage from '../../../storageMock';
import {
  items,
  item,
  user,
} from '../testdata/common';

beforeEach(() => {
  window.localStorage = localStorage;
  fetch.resetMocks();
});

afterEach(() => {
  localStorage.clear();
});

describe('Services - SHOPPINGLIST', () => {
  describe('getItems', () => {
    it('returns array with "error" as only element if response status is not 200-300', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: items }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const result = await shoppinglistService.getItems();
      expect(fetch.mock.calls.length).toBe(1);
      expect(result).toEqual(['Error']);
    });

    it('returns an array containing all items that are found', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: items }),
        {
          status: 200,
        },
      );

      const result = await shoppinglistService.getItems();
      expect(fetch.mock.calls.length).toBe(1);
      expect(result.data).toEqual(items);
    });
  });

  describe('postItem', () => {
    it('returns "error" if response status is not 200-300', async () => {
      localStorage.setItem('user', JSON.stringify(user));
      fetch.mockResponseOnce(
        JSON.stringify({ data: items }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const result = await shoppinglistService.postItem({ item });
      expect(fetch.mock.calls.length).toBe(1);
      expect(result).toEqual('Error');
    });

    it('returns the server response on success', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: item }),
        {
          status: 200,
        },
      );

      const result = await shoppinglistService.postItem();
      expect(fetch.mock.calls.length).toBe(1);
      expect(result.data).toEqual(item);
    });
  });

  describe('deleteItem', () => {
    it('returns "error" if response status is not 200-300', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: items }),
        {
          status: 404,
          headers: { 'content-type': 'application/json' },
        },
      );

      const result = await shoppinglistService.deleteItem({ id: 1 });
      expect(fetch.mock.calls.length).toBe(1);
      expect(result).toEqual('Error');
    });

    it('returns the server response on success', async () => {
      fetch.mockResponseOnce(
        JSON.stringify({ data: item }),
        {
          status: 200,
        },
      );

      const result = await shoppinglistService.deleteItem({ id: 1 });
      expect(fetch.mock.calls.length).toBe(1);
      expect(result.data).toEqual(item);
    });
  });
});
