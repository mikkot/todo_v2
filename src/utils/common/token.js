const getToken = () => {
  let token = localStorage.getItem('token');
  token = token ? JSON.parse(token) : token;
  return token;
};

export { getToken };
