/**
 *
 * @param {String} url The connection url
 * @returns {WebSocket} The Websocket
 */
const getWebSocket = (url) => new WebSocket(url || 'ws://127.0.0.1:3030');

const setOnMessage = (callback, webSocket) => {
  webSocket.onmessage = (event) => { // eslint-disable-line
    console.log('calling WebSocket callback', event.message && event.message.data);
    callback();
  };
};

export {
  setOnMessage,
  getWebSocket,
};
