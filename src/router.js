import Vue from 'vue';
import Router from 'vue-router';
import Tasks from './views/Tasks.vue';
import Shopping from './views/Shopping.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Shopping,
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: Tasks,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
    },
    {
      path: '/signup',
      name: 'signup',
      component: () => import('./views/Signup.vue'),
    },
    {
      path: '/shopping*',
      name: 'shoppinglist',
      component: () => import('./views/Shopping.vue'),
    },
    {
      path: '/batch',
      name: 'batchofshoppingitems',
      component: () => import('./views/ShoppingBatch.vue'),
    },
    // Add 404 route
    {
      path: '*',
      name: '404',
      component: () => import('./views/404.vue'),
    },
  ],
});
