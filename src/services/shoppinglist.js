// import { status, json } from './helpers/resultHandler';
import { baseUrl } from './helpers/config';
import { getToken } from '../utils/common/token';

const url = `${baseUrl}shoppinglist`;

const getItems = async () => {
  let result;

  try {
    const token = getToken();

    result = await fetch(url, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (result.status >= 200 && result.status < 300) {
      return await result.json();
    }
    result = ['Error'];
  } catch (error) {
    console.error('Error in shoppinglist getItems', error);
    result = ['Error'];
  }

  return result;
};

const postItems = async (payload) => {
  let result;

  try {
    const token = getToken();

    // Format the items to an Array of objects [{ name: String }, ...]
    // The backend will add the user to the items before saving them.
    let items = Array.isArray(payload) ? payload : [payload];
    items = items.map((item) => ({
      name: item.name,
      store: item.store,
    }));

    result = await fetch(url, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        items,
      }),
    });

    if (result.status >= 200 && result.status < 300) {
      result = await result.json();
    } else {
      result = 'Error';
    }
  } catch (error) {
    console.log('Error in shoppinglist postItem', error);
    result = 'Error';
  }

  return result;
};

const deleteItem = async (payload) => {
  let result;

  try {
    const token = getToken();

    result = await fetch(`${url}/${payload.id}`, {
      method: 'delete',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (error) {
    console.log('Error in shoppinglist deleteItem', error);
    result = 'Error';
  }

  if (result.status >= 200 && result.status < 300) {
    result = await result.json();
  } else {
    result = 'Error';
  }

  return result;
};

const updateItem = async (payload) => {
  let result;

  try {
    const token = getToken();

    const { item } = payload;

    result = await fetch(`${url}/${payload.id}`, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ item }),
    });
  } catch (error) {
    console.error('Error in shoppinglist updateItem', error);
    result = 'Error';
  }

  if (result.status >= 200 && result.status < 300) {
    result = await result.json();
  } else {
    result = 'Error';
  }

  return result;
};

const getItem = async (itemId) => {
  console.log('Shoppinglistservice getItem ', itemId);
  let result;

  try {
    const token = getToken();
    result = await fetch(`${url}/${itemId}`, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (error) {
    console.error('Error in shoppinglist getItem', error);
    result = 'Error';
  }

  if (result.status >= 200 && result.status < 300) {
    result = await result.json();
  } else {
    result = 'Error';
  }

  return result;
};

const getBatches = async () => {
  let result;

  try {
    const token = getToken();

    result = await fetch(`${baseUrl}shoppingbatch`, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    if (result.status >= 200 && result.status < 300) {
      result = await result.json();
    } else {
      result = ['Error'];
    }
  } catch (error) {
    console.error('Error retrieving ShoppingItem Batches');
    result = ['Error'];
  }

  return result;
};

const postBatch = async (payload) => {
  let result;
  if (!payload.batch) return 'Error';

  try {
    const token = getToken();
    const { batch } = payload;

    result = await fetch(`${baseUrl}shoppingbatch`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        batch,
      }),
    });

    if (result.status >= 200 && result.status < 300) {
      result = await result.json();
    } else {
      result = 'Error';
    }
  } catch (error) {
    console.error('Error saving ShoppingItem Batches');
    result = 'Error';
  }

  return result;
};

const deleteBatch = async (payload) => {
  let result;
  if (!payload.id) return 'Error';

  try {
    const token = getToken();

    result = await fetch(`${baseUrl}shoppingbatch/${payload.id}`, {
      method: 'delete',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (error) {
    console.error('Error in shoppinglist deleteBatch', error);
    result = 'Error';
  }

  if (result.status >= 200 && result.status < 300) {
    result = await result.json();
  } else {
    result = 'Error';
  }

  return result;
};

export default {
  getItems,
  postItems,
  deleteItem,
  updateItem,
  getBatches,
  postBatch,
  deleteBatch,
  getItem,
};
