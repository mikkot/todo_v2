/* eslint-disable import/prefer-default-export */
export const baseUrl = process.env.NODE_ENV === 'production'
  ? 'https://todo-shoppinglist.herokuapp.com/api/' : 'http://localhost:3030/api/';
