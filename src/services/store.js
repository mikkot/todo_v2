import { baseUrl } from './helpers/config';
import { getToken } from '../utils/common/token';

const url = `${baseUrl}store`;

const addStore = async (data) => {
  console.log('Store data: ', data);
  let result;

  try {
    result = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify({ ...data }),
    });
  } catch (error) {
    return error.status;
  }

  result = await result.json();
  return result;
};

const getStores = async () => {
  let result;

  try {
    result = await fetch(url, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    });
  } catch (error) {
    return error.status;
  }

  // Error checking
  result = await result.json();
  return result;
};

export { addStore, getStores };
