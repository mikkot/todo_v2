import { baseUrl } from './helpers/config';

const url = `${baseUrl}task`;

const postTask = async (payload) => {
  let result;

  try {
    const user = JSON.parse(localStorage.getItem('user'));
    const token = JSON.parse(localStorage.getItem('token'));
    result = await fetch(url, {
      method: 'post',
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        task: {
          priority: payload.priority,
          label: payload.label,
          description: payload.description,
          user: user ? user.id : null,
        },
      }),
    });

    if (result.status >= 200 && result.status < 300) {
      result = result.json();
    } else {
      result = 'Error';
    }
    return result;
  } catch (error) {
    console.log('Error in postTask', error);
  }

  return 'Error';
};

const deleteTask = async (payload) => {
  let result;

  try {
    const token = JSON.parse(localStorage.getItem('token'));
    result = await fetch(`${url}/${payload.id}`, {
      method: 'delete',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (error) {
    console.log('Error in postTask', error);
  }
  if (result.status >= 200 && result.status < 300) {
    result = 'Success';
    return result;
  }

  return 'Error';
};

const getTask = async () => {
  let result;

  try {
    const token = JSON.parse(localStorage.getItem('token'));
    result = await fetch(url, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (error) {
    console.log('Error in postTask', error);
  }

  if (result.status >= 200 && result.status < 300) {
    result = await result.json();
  } else {
    result = 'Error';
  }

  return result;
};

const getTasks = async () => {
  let result;

  try {
    const token = JSON.parse(localStorage.getItem('token'));
    result = await fetch(url, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  } catch (error) {
    console.log('Error in postTask', error);
    return 'Error';
  }

  if (result.status >= 200 && result.status < 300) {
    result = await result.json();
  } else {
    result = 'Error';
  }

  return result;
};

const updateTask = async (payload) => {
  let result;

  try {
    const token = JSON.parse(localStorage.getItem('token'));
    const { task } = payload;
    result = await fetch(`${url}/${payload.id}`, {
      method: 'put',
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ task }),
    });
  } catch (error) {
    console.error(error);
    return error;
  }

  if (result.status >= 200 && result.status < 300) {
    return result.status;
  }

  return `Error - ${result && result.status}`;
};

export default {
  postTask, getTask, getTasks, deleteTask, updateTask,
};
