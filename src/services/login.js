// import { status, json } from './helpers/resultHandler';
import { baseUrl } from './helpers/config';

const url = `${baseUrl}login`;// `${baseUrl}login`;

const postLogin = async (credentials) => {
  let result;

  try {
    result = await fetch(url, {
      method: 'post',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        username: credentials.username, password: credentials.password,
      }),
    });
    if (result.status >= 200 && result.status < 300) {
      result = result.json();
    } else {
      result = 'Error';
    }
  } catch (error) {
    console.log('Error in postLogin', error);
  }

  return result;
};

export default { postLogin };
